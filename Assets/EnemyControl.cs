﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControl : MonoBehaviour
{
    public float speed;
    public float xMinPos;
    public float xMaxPos;
    public int directionX;
    public PoolManager objPool;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    void restar()
    {
        if(transform.position.x>=xMaxPos)
        {
            objPool.SetObject(this.gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
        transform.position = new Vector3(transform.position.x + speed * directionX * Time.deltaTime, transform.position.y, transform.position.z);
        transform.rotation = Quaternion.Euler(transform.rotation.x, +90, transform.rotation.z);
        restar();
    }
    void SetDir()
    {
        directionX = 1;
    }
    public void SetInitialValues(PoolManager manager)
    {
        objPool = manager;
        SetDir();
    }


}
