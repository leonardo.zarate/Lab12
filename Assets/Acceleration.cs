﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Acceleration : MonoBehaviour
{
    private Vector3 acceleration;
    private new Rigidbody rigidbody;

    public float speed = 10f;


    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        acceleration = new Vector3(Input.acceleration.x, Input.acceleration.y, Input.acceleration.y);

        float mov = Input.acceleration.z;

        if (mov != 0.0f)
        {
            Vector3 dir = transform.forward * mov + transform.right;

            rigidbody.MovePosition(acceleration + dir * speed * Time.deltaTime);
        }


        


    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Wall")
        {
            Debug.Log("cuidado");
        }
    }

}
