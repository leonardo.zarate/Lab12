﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour
{
    public GameObject prebad;
    public int MaxQuant;
    public List<GameObject> objectPool;

    // Start is called before the first frame update
    void Start()
    {
        Instantiate();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void Instantiate()
    {
        GameObject tmp;
        for(int i = 0; i<MaxQuant; ++i)
        {
            tmp= Instantiate(prebad, transform.position, transform.rotation);
            objectPool.Add(tmp);
            tmp.SetActive(false);
            tmp.transform.SetParent(this.transform);

        }
    }
    public void GetObject()
    {
        if (objectPool.Count > 0)
        {

            for (int i = 0; i < MaxQuant; i++)
            {
                GameObject tmp = objectPool[i];
                objectPool.RemoveAt(i);
                tmp.SetActive(true);
                tmp.GetComponent<EnemyControl>().SetInitialValues(this);
            }
        }
        else
        {
            Debug.Log("No mas cosas");
        }


    }
    public void SetObject(GameObject objectsaved)
    {
        objectPool.Add(objectsaved);
        objectsaved.SetActive(false);
    }
}
